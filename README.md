# apiusuarios
Desarrollo de una aplicación que exponga una API RESTful de creación de usuarios.

## Documentación
http://localhost:8080/swagger-ui.html

1. La apiusurios integra seguridad con el uso de Token por lo tanto debe generarse un token previamente, se recomiena el uso de Postam

POST: localhost:8080/login

Ejemplo repuesta:

{    
    "token": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzb2Z0dGVrSldUIiwic3ViIjoic2F5YW1pdG8iLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjkzMjM0MTEzLCJleHAiOjE2OTMyMzQ3MTN9.lohTxRghF5CPK0CWIdNaQXVqt2rBS5KNkCFC09BP5ko351KcY5B6yV3Mw-W_pn3R-MbULo4V21pt3OmbqSwvTg",
}

2. En el Header envie el token de la siguiente forma

Authorization: "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzb2Z0dGVrSldUIiwic3ViIjoic2F5YW1pdG8iLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjkzMjM0MTEzLCJleHAiOjE2OTMyMzQ3MTN9.lohTxRghF5CPK0CWIdNaQXVqt2rBS5KNkCFC09BP5ko351KcY5B6yV3Mw-W_pn3R-MbULo4V21pt3OmbqSwvTg"

3. Registrar un usuarios

POST: localhost:8080/newuser

Ejemplo Json:

{
        "name": "Sandra Moreno4",
        "email": "sayamito541@gmail.com",
        "password": "Saya1803*",
        "lastLogin": null,        
        "phones": [
            {
                "number": 3125153910,
                "cityCode": 57,
                "contryCode": 1
            }
        ]
}


Relice las validaciones consideradas en la prueba

4. Puede pasar que si pasa mucho tiempo el token ya no va a funcionar

Ejemplo expiro token: 

"message": "JWT expired at 2023-08-28T09:18:38Z. Current time: 2023-08-28T09:30:18Z, a difference of 700832 milliseconds.  Allowed clock skew: 0 milliseconds."

@autor Sandra Moreno

