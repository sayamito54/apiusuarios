package com.nisum.users.models.services;

import java.util.List;

import com.nisum.users.models.entity.User;

public interface IUserService {
	
	public List<User> findAll();
	
	public User save(User user);
	
	public boolean existeCorreo(String email );

}
