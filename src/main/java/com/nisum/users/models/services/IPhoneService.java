package com.nisum.users.models.services;

import java.util.List;
import com.nisum.users.models.entity.Phone;


public interface IPhoneService {
	
	public List<Phone> findAll();	
	
	public Phone save(Phone phone);

}
