package com.nisum.users.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nisum.users.models.dao.IPhoneDao;
import com.nisum.users.models.entity.Phone;

@Service
public class PhoneServiceImpl implements IPhoneService {
	
	@Autowired
	private IPhoneDao phoneDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Phone> findAll() {		
		return (List<Phone>) phoneDao.findAll();		
	}

	@Override
	public Phone save(Phone phone) {
		return phoneDao.save(phone);
	}


}
