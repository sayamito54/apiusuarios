package com.nisum.users.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nisum.users.models.dao.IUserDao;
import com.nisum.users.models.entity.User;


@Service
public class UserServiceImpl implements IUserService, UserDetailsService{
	
	@Autowired
	private IUserDao userDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<User> findAll() {		
		return (List<User>) userDao.findAll();
	}

	@Override
	@Transactional
	public User save(User user) {		
		return userDao.save(user);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existeCorreo(String email) {
		boolean existe = false;
		userDao.findByEmail(email);
		if (userDao.findByEmail(email).size()>0) {
			existe=  true;
		}
		return existe;
	}
	
	
    
}
