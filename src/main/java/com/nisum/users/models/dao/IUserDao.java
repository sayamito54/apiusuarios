package com.nisum.users.models.dao;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

import com.nisum.users.models.entity.User;

public interface IUserDao extends CrudRepository<User, Long>{
	
	List<User> findByEmail(String email);

}
