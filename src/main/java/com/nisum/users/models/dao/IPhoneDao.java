package com.nisum.users.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.nisum.users.models.entity.Phone;

public interface IPhoneDao extends CrudRepository<Phone, Long>{

}
