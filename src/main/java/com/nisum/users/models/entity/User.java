package com.nisum.users.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.checkerframework.common.aliasing.qual.Unique;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.nisum.users.util.ValidPassword;
import com.nisum.users.util.VistaJson;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="USERS")
public class User implements Serializable{
	
	@JsonView({VistaJson.VistaNombre.class})
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;	
	
	@NotEmpty	
	private String name;
	
	@NotEmpty
	@Email		
	private String email;
	
	@NotEmpty
	@Size(min=4,max=12)
	@ValidPassword	
	private String password;		
	
	@JsonView({VistaJson.VistaNombre.class})
	@Temporal(TemporalType.DATE)	
	private Date created;
	
	@JsonView({VistaJson.VistaNombre.class})
	@Temporal(TemporalType.DATE)	
	private Date modified;
	
	@JsonView({VistaJson.VistaNombre.class})
	@Column(name="last_login")
	@Temporal(TemporalType.DATE)
	private Date lastLogin;	
	
	@JsonView({VistaJson.VistaNombre.class, VistaJson.VistaToken.class})
	private String token;
	
	@JsonView({VistaJson.VistaNombre.class})
	private boolean isactive;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Phone> phones;		
	

	@PrePersist
	public void prePersist() {
		created = new Date();		
		lastLogin = new Date();		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}	

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isIsactive() {
		return isactive;
	}

	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}	

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}



	private static final long serialVersionUID = 1L;
}
