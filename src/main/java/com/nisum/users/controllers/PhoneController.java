package com.nisum.users.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nisum.users.models.entity.Phone;
import com.nisum.users.models.entity.User;
import com.nisum.users.models.services.IPhoneService;
import com.nisum.users.models.services.IUserService;

@RestController
public class PhoneController {
	
	@Autowired
	private IPhoneService phoneService;
	
	@RequestMapping("phones")	
	public List<Phone> index(){		
		return phoneService.findAll();		
	}

}
