package com.nisum.users.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.validation.BindingResult;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import com.fasterxml.jackson.annotation.JsonView;
import com.nisum.users.models.entity.Phone;
import com.nisum.users.models.entity.User;
import com.nisum.users.models.services.IPhoneService;
import com.nisum.users.models.services.IUserService;
import com.nisum.users.util.VistaJson;

@RestController
public class UserController {
	
	private String token;

	@Autowired
	private IUserService userService;
	@Autowired
	private IPhoneService phoneService;

	@GetMapping("/users")
	public List<User> index() {
		return userService.findAll();
	}

	@PostMapping("/login")
	@JsonView({VistaJson.VistaToken.class})
	public User login(@RequestParam("user") String username, @RequestParam("password") String pwd) {

		String token = getJWTToken(username);
		User user = new User();
		user.setEmail(username);
		user.setToken(token);
		return user;

	}

	@PostMapping("/newuser")
	@ResponseStatus(HttpStatus.CREATED)
	@JsonView({VistaJson.VistaNombre.class})
	public ResponseEntity<?> create(@RequestBody @Valid User user, BindingResult bindingResult) {

		User newUser = new User();
		List<Phone> phones = new ArrayList();
		Map<String, Object> response = new HashMap<>();
		
		if(userService.existeCorreo(user.getEmail())) {
			response.put("mensaje", "Error la realizar el insert de datos");response.put("mensaje", "El correo ya se encuentra ingresado");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_ACCEPTABLE);
		}

		if (bindingResult.hasErrors()) {

			List<String> errors = bindingResult.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("mensaje", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			phones = user.getPhones();
			newUser.setName(user.getName());
			newUser.setEmail(user.getEmail());
			newUser.setPassword(user.getPassword());
			newUser.setIsactive(true);
			newUser.setToken(token);
			userService.save(newUser);
			if (newUser.getId() != null) {
				for (Phone p : phones) {
					p.setUser(newUser);
					phoneService.save(p);
				}
			}

		} catch (DataAccessException e) {
			response.put("mensaje", "Error la realizar el insert de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		response.put("mensaje",
				"El usuario ID: ".concat(newUser.getId().toString().concat(" se guardo satisfactoriamente")));
		response.put("User", newUser);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		//String 
		token = Jwts.builder().setId("softtekJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}
